# -*- coding: utf-8 -*-
from telegram.ext import CommandHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
import texts
import os

from tgmagic.buttons import PrevButton
from tgmagic.bot import MagicFunction
from tgmagic.helper import menu

my_custom_menu = {
    'menu': {
        'iphone': '📱iPhone',
        'iphone_nested': {
            'iphone7': 'Iphone 7',
            'iphone7_nested': {
                'buy_7_32': '✅Iphone 7 32 Gb: $269, €243',
                'buy_7_64': '✅Iphone 7 64 Gb: $293 €261',
                'buy_7_128': '✅Iphone 7 128 Gb: $326 €282',
                'prev': PrevButton('◀️Назад')
            },
            'iphone7plus': 'Iphone 7 Plus',
            'iphone7plus_nested': {
                'buy_7plus_32': '✅Iphone 7 Plus 32 Gb: $342, €298',
                'buy_7plus_64': '✅Iphone 7 Plus 64 Gb: $359 €315',
                'buy_7plus_128': '✅Iphone 7 Plus 128 Gb: $371 €327',
                'prev': PrevButton('◀️Назад')
            },
            'iphone8': 'Iphone 8',
            'iphone8_nested': {
                'buy_8_32': '✅Iphone 8 32 Gb: $392, €348',
                'buy_8_128': '✅Iphone 8 128 Gb: $422 €378',
                'prev': PrevButton('◀️Назад')
            },
            'iphone8plus': 'Iphone 8 Plus',
            'iphone8plus_nested': {
                'buy_8plus_64': '✅Iphone 8 64 Gb: $447, €402',
                'buy_8plus_128': '✅Iphone 8 128 Gb: $479 €435',
                'prev': PrevButton('◀️Назад')
            },
            'iphoneX': 'Iphone X',
            'iphoneX_nested': {
                'buy_X_64': '✅Iphone X 64 Gb: $512, €465',
                'buy_X_128': '✅Iphone X 128 Gb: $535 €486',
                'buy_X_256': '✅Iphone X 256 Gb: $565 €512',
                'prev': PrevButton('◀️Назад')
            },
            'iphoneXS': 'Iphone XS',
            'iphoneXS_nested': {
                'buy_XS_64': '✅Iphone XS 64 Gb: $675, €611',
                'buy_XS_128': '✅Iphone XS 128 Gb: $700 €634',
                'buy_XS_256': '✅Iphone XS 256 Gb: $740 €670',
                'buy_XS_512': '✅Iphone XS 512 Gb: $785 €705',
                'prev': PrevButton('◀️Назад')
            },
            # 'iphoneXSmax': 'Iphone XS Max',
            # 'iphoneXSmax_nested': {
            # 'prev': PrevButton('◀️Назад')
            # },
            'prev': PrevButton('◀️Назад...'),
        },
        'ipad': '⌨️iPad',
        'ipad_nested': {
            'buy_ipad_32': '✅iPad (2018) WiFi + cellular 32 Gb: $313, €283',
            'buy_ipad_128': '✅iPad (2018) WiFi + cellular 128 Gb: $402, €364',
            'buy_ipadpro_64': '✅iPad Pro 10,5 WiFi + cellular 64: $432, €391',
            'buy_ipadpro_256': '✅iPad Pro 10,5 WiFi + cellular 256: $582, €527',
            'buy_ipadpro_512': '✅iPad Pro 10,5 WiFi + cellular 512: $641, €581',
            'prev': PrevButton('◀️Назад')
        },

        'macbook': '💻MacBook',
        'macbook_nested': {
            'buy_macbook_1': '✅MacBook Pro 13 core i5, 2,3 gHz, 8g 128 SSD, iris Plus 655: $776, €702',
            'buy_macbook_2': '✅MacBook Pro 13 core i5, 2,3 gHz, 8g 256 SSD, iris Plus 655: $910, €824',
            'buy_macbook_3': '✅MacBook Pro 13 core i5, 3,1 gHz, 8g 512 SSD, iris Plus 655: $1104, €1000',
            'buy_macbook_4': '✅MacBook Pro 13 core i5, 3,3 gHz, 16g 512g SSD, iris Plus 655: $1238, €1121',
            'buy_macbook_5': '✅MacBook Pro 13 core i5, 2,3 gHz, 16g 512g SSD, iris Plus 655: $1373, €1243',
            'buy_macbook_6': '✅MacBook Pro 13 core i7, 2,7 gHz, 16g 1Tb SSD, iris Plus 655: $1701, €1541',
            'buy_macbook_7': '✅MacBook Pro 13 core i9, 2,9 gHz, 32g 1Tb SSD, Radeon Pro 560X: $2507, €2270',
            'prev': PrevButton('◀️Назад')
        },

        'support': '👩🏼‍💼Консультант',
        #'support_nested': {
        #},

        'service': '🗜Сервис',
        #'service_nested': {
        #},

        'payment': '💵Оплата',
        #'payment_nested': {
        #},

        'delivery': '📦Доставка',
        #'delivery_nested': {
        #},

        'faq': '💬FAQ',
        #'faq_nested': {
        #},
    }
}

class TestBot(MagicFunction):
    def start(self, bot, update):
        bot.sendMessage(
            text= texts.start,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def prev(self, bot, update):
        bot.sendMessage(
            text='Возращаемся назад...',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def payment(self, bot, update):
        bot.sendMessage(
            text='''Мы принимаем к оплате: \n ✅PayPal; \n ✅Bitcoin; \n ✅Sberbank.''',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def delivery(self, bot, update):
        bot.sendMessage(
            text='''Мы доставляем по всей России.''',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def service(self, bot, update):
        bot.sendMessage(
            text='''Есть.''',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def support(self, bot, update):
        bot.sendMessage(
            text='''Спит.''',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def faq(self, bot, update):
        bot.sendMessage(
            text=texts.faq,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphone(self, bot, update):
        bot.sendMessage(
            text='Выберите желаемую модель...',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphone7(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphone7plus(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphone8(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphone8plus(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphoneX(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def iphoneXS(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def ipad(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие объемы памяти и модели:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def macbook(self, bot, update):
        bot.sendMessage(
            text='Доступны следующие модели:',
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7_32(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 32 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7plus_32(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 Plus 32 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7plus_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 Plus 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_7plus_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 7 Plus 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_8_32(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 8 32 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_8_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 8 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_8plus_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 8 Plus 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_8plus_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone 8 Plus 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_X_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone X 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_X_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone X 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_X_256(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone X 256 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_XS_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone XS 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_XS_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone XS 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_XS_256(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone XS 256 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_XS_512(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести Iphone XS 512 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_ipad_32(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести iPad (2018) WiFi + cellular 32 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_ipad_128(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести iPad (2018) WiFi + cellular 128 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_ipadpro_64(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести iPad Pro 10,5 WiFi + cellular 64 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_ipadpro_256(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести iPad Pro 10,5 WiFi + cellular 256 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )


    def buy_ipadpro_512(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести iPad Pro 10,5 WiFi + cellular 512 Gb, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_1(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i5, 2,3 gHz, 8g 128 SSD, iris Plus 655, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_2(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i5, 2,3 gHz, 8g 256 SSD, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_3(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i5, 3,1 gHz, 8g 512 SSD, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_4(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i5, 3,3 gHz, 16g 512g SSD, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_5(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i5, 2,3 gHz, 16g 512g SSD, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_6(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i7, 2,7 gHz, 16g 1Tb SSD, iris Plus 655, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    def buy_macbook_7(self, bot, update):
        bot.sendMessage(
            text='Для того, чтобы преобрести MacBook Pro 13 core i9, 2,9 gHz, 32g 1Tb SSD, Radeon Pro 560X, обратитесь к нашему менеджеру: %s' % texts.manager,
            chat_id=update.message.chat_id,
            reply_markup=self.gen_keyboard(update)
        )

    @menu
    def text(self, bot, update):
        # ...
        # and any other handlers
        pass

    def main(self):
        self.set_custom_menu(my_custom_menu)

        updater = Updater("787406161:AAHhniBSc8-Y3fcBhH_yX6JFDRNSSj7K6Dw",
                      request_kwargs={
                          'proxy_url': 'HTTPS://93.170.117.230:44000',
                      })

        dp = updater.dispatcher
        dp.add_handler(CommandHandler('start', self.start))
        dp.add_handler(MessageHandler(Filters.text, self.text))
        updater.start_polling()

TestBot().main()